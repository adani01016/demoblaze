package hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;
import userInterfaces.HomeUI;

public class TheActor {

    HomeUI urlPortal;
    public static Actor testActor;

    @Managed
    private static WebDriver driver;

    public static WebDriver getDriver() {
        return driver;
    }

    @Before(order = 1)
    public void setTheStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Before(order = 2)
    public void setTheActor(){
        testActor = Actor.named("testActor");
        testActor.can(BrowseTheWeb.with(driver));
        testActor.wasAbleTo(Open.browserOn().the(urlPortal));
    }

    @After
    public void teardown() {
        if (driver != null) {
            driver.quit();
        }
    }

}