package questions;

import net.serenitybdd.screenplay.Question;
import userInterfaces.CarritoUI;

public class SeValidaCarrito {

    private SeValidaCarrito(){
    }

    public static Question<Boolean> carritoLleno() {
        return actor -> CarritoUI.BUTTON_DELETE.resolveFor(actor).isCurrentlyEnabled();
    }
}
