package tasks;


import hooks.TheActor;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;
import userInterfaces.*;
import utils.VentanaAlerta;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class RegistroUsuario implements Task {

    private final String userName;
    private final String passWord;
    private static String alertText;

    public RegistroUsuario(String userName, String passWord) {
        this.userName = userName;
        this.passWord = passWord;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(HomeUI.LINK_SINGUP),
                WaitUntil.the(RegistroUI.USUARIO, isVisible()).forNoMoreThan(5).seconds(),
                Enter.theValue(userName).into(RegistroUI.USUARIO),
                Enter.theValue(passWord).into(RegistroUI.CONTRASENA),
                //SendKeys.of(Keys.TAB).into(registroUI.BTN_CREAR)
                Click.on(RegistroUI.BTN_CREAR)
        );
        VentanaAlerta alert = new VentanaAlerta(TheActor.getDriver());
        alertText = alert.getAndCloseAlertText();
    }

    public static String getAlertText(){
        return alertText;
    }

    public static RegistroUsuario registroUsuario(String userName, String passWord){
        return new RegistroUsuario(userName,passWord);
    }
}