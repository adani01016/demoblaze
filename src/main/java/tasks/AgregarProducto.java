package tasks;

import hooks.TheActor;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.junit.Assert;
import userInterfaces.HomeUI;
import utils.VentanaAlerta;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class AgregarProducto implements Task {
    private String num;

    public AgregarProducto(String num) {
        this.num = num;
    }

    public static AgregarProducto agregarProducto(String num){
        return new AgregarProducto(num);
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(HomeUI.CARD_PRODUCT.of(num)),
                WaitUntil.the(HomeUI.BUTTON_ADD_TO_CART, isVisible()).forNoMoreThan(6).seconds(),
                Click.on(HomeUI.BUTTON_ADD_TO_CART)
        );
        VentanaAlerta alert = new VentanaAlerta(TheActor.getDriver());
        String alertText = alert.getAlertText();
        Assert.assertEquals("Product added", alertText);
        actor.attemptsTo(
                Click.on(HomeUI.BUTTON_HOME),
                WaitUntil.the(HomeUI.BANNER, isVisible()).forNoMoreThan(5).seconds()
        );
    }
}
