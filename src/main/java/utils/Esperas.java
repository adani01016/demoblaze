package utils;


import hooks.TheActor;

import java.util.concurrent.TimeUnit;

public class Esperas {
    public static void TwoSecond(){

        TheActor.getDriver().manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }
    public static void ThreeSecond() {
        TheActor.getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    public static void fivs(){
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

