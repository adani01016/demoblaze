package userInterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class RegistroUI {
    public static final Target USUARIO = Target.the("Campo para ingresar el usaurio").located(By.id("sign-username"));
    public static final Target CONTRASENA = Target.the("Campo para ingresar la contraseña").located(By.id("sign-password"));
    public static final Target BTN_CREAR = Target.the("Boton para crear el usuario").located(By.xpath("//*[@id=\"signInModal\"]/div/div/div[3]/button[2]"));
}

