package userInterfaces;


import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("page:webdriver.base.url")

public class HomeUI extends PageObject {

    public static final Target LINK_SINGUP = Target.the("Link ubicado en el header para crear usuario").located(By.id("signin2"));
    public static final Target CARD_PRODUCT = Target.the("Tarjeta del producto").locatedBy("//*[@id=\"tbodyid\"]/div[{0}]/div/div/h4");
    public static final Target BUTTON_HOME = Target.the("Boton Home").located(By.xpath("//*[@id=\"navbarExample\"]/ul/li[1]/a"));
    public static final Target BUTTON_CART = Target.the("Boton Cart").located(By.xpath("//*[@id=\"navbarExample\"]/ul/li[4]/a"));
    public static final Target BANNER = Target.the("Banner del Home").located(By.id("carouselExampleIndicators"));
    public static final Target BUTTON_ADD_TO_CART = Target.the("Boton para agregar producto al carrito").located(By.xpath("//*[@id=\"tbodyid\"]/div[2]/div/a"));

}
