Feature: Registrar un usuario en el portal DEMOBLAZE

  @NewUser
  Scenario Outline: nuevo usuario - registro exitoso
    Given Ingresa a la pagina web
    When Ingresa a Sign up y carga las credenciales "<username>""<password>"
    Then Se debera visualizar un mensaje de Sign up successful.

    Examples:
      | username  | password   |
      |   AB2223  |   ab123    |
