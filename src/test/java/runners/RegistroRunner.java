package runners;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberSerenityRunner;
import org.junit.runner.RunWith;

@RunWith(CucumberSerenityRunner.class)
@CucumberOptions(
        features = "src/test/resources/features/Registro.feature",
        glue = {"stepDefinitions", "hooks"},
        snippets = CucumberOptions.SnippetType.CAMELCASE
)
public class RegistroRunner {
}
