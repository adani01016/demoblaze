package stepDefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.actions.Click;
import userInterfaces.HomeUI;
import java.util.List;

import static hooks.TheActor.testActor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;
import static questions.SeValidaCarrito.carritoLleno;
import static tasks.AgregarProducto.agregarProducto;

public class agregarProdCarritoStep {

    @When("Agregar productos al carrito")
    public void agregarProductosAlCarrito(DataTable table) {
        List<List<String>> rows = table.asLists(String.class);
        for (List<String> columns : rows){
            testActor.attemptsTo(agregarProducto(columns.get(0)));
        }
    }

    @Then("Se debera visualizar el carrito con productos")
    public void seDeberaVisualizarElCarritoConProductos() {
        testActor.attemptsTo(Click.on(HomeUI.BUTTON_CART));
        testActor.should(seeThat("El carrito tiene productos", carritoLleno(), equalTo(true)));
    }

}