package stepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import tasks.RegistroUsuario;

import static hooks.TheActor.testActor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;
import static tasks.RegistroUsuario.registroUsuario;

public class registroUsuarioStep {

    @Given("Ingresa a la pagina web")
    public void ingresaALaPaginaWeb() {
    }

    @When("Ingresa a Sign up y carga las credenciales {string}{string}")
    public void ingresaASignUpYCargaLasCredenciales(String usu, String contr) {
        testActor.attemptsTo(registroUsuario(usu, contr));
    }

    @Then("Se debera visualizar un mensaje de Sign up successful.")
    public void seDeberaVisualizarUnMensajeDeSignUpSuccessful() {
        String alertText = RegistroUsuario.getAlertText();
        testActor.should(seeThat("El mensaje de alerta es correcto", actual -> alertText, equalTo("Sign up successful.")));
    }

}
