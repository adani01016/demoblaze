#!/bin/bash

# Define la ubicación de tu proyecto
#PROJECT_DIR="src/test/java/runners/ElementTextBoxRunner.java"

# Navega al directorio de tu proyecto
#cd "$PROJECT_DIR"

# Ejecuta la construcción de Gradle para asegurarte de que todas las dependencias estén resueltas
#./gradlew clean build
#./gradlew clean test --tests "runners.ElementTextBoxRunner" --info --stacktrace -Denvironment=chrome
./gradlew test --tests runners.AgregarProdCarritoRunner -Denvironment=edge
# Ejecuta tus pruebas de Cucumber con Gradle
#./gradlew cucumber
